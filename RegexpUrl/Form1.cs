﻿using System;
using System.Net;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RegexpUrl
{
    public partial class FMain : Form
    {
        const string RefPattern = @"((http|ftp|https):\/\/|www.)[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=% &amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?";
        public FMain()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Объект запроса
            //HttpWebRequest rew = (HttpWebRequest)WebRequest.Create("http://robotstxt.org.ru/rurobots");
            HttpWebRequest rew = (HttpWebRequest)WebRequest.Create(tbUrl.Text);

            // Отправить запрос и получить ответ
            HttpWebResponse resp = (HttpWebResponse)rew.GetResponse();

            // Получить поток
            Stream str = resp.GetResponseStream();

            // Выводим в TextBox
            int ch;
            string message = "";
            for (int i = 1; ; i++)
            {
                ch = str.ReadByte();
                if (ch == -1) break;
                message += (char)ch;
            }

            my_Text.Text = message;

            // Закрыть поток
            str.Close();
        }


        private void DumpHrefs(string InputString)
        {
            try
            {
                Match m = Regex.Match(InputString, RefPattern, RegexOptions.IgnoreCase | RegexOptions.Compiled, TimeSpan.FromSeconds(1));
                int cnt = 0;
                while (m.Success)
                {

                    // MessageBox.Show(m.ToString());
                    showUri.Text += m.ToString() + "\r\n";
                    m = m.NextMatch();
                    cnt++;

                }
            }
            catch (RegexMatchTimeoutException)
            {
                Console.WriteLine("The matching operation Timed out");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DumpHrefs(my_Text.Text);
        }

        private void tbUrl_TextChanged(object sender, EventArgs e)
        {
            Regex rgx = new Regex(RefPattern);
            if (rgx.IsMatch(tbUrl.Text))
            {
                bShow.Enabled = true;
                bFindOutUrl.Enabled = true;
                label2.Text = "";
            }
            else
            {
                bShow.Enabled = false;
                bFindOutUrl.Enabled = false;
                label2.Text = "It's not a correct url!";
            }
        }
    }
}
