﻿namespace RegexpUrl
{
    partial class FMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.bShow = new System.Windows.Forms.Button();
            this.my_Text = new System.Windows.Forms.TextBox();
            this.showUri = new System.Windows.Forms.TextBox();
            this.bFindOutUrl = new System.Windows.Forms.Button();
            this.tbUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // bShow
            // 
            this.bShow.Location = new System.Drawing.Point(353, 13);
            this.bShow.Name = "bShow";
            this.bShow.Size = new System.Drawing.Size(181, 37);
            this.bShow.TabIndex = 0;
            this.bShow.Text = "ShowHtml";
            this.bShow.UseVisualStyleBackColor = true;
            this.bShow.Click += new System.EventHandler(this.button1_Click);
            // 
            // my_Text
            // 
            this.my_Text.Location = new System.Drawing.Point(57, 56);
            this.my_Text.Multiline = true;
            this.my_Text.Name = "my_Text";
            this.my_Text.Size = new System.Drawing.Size(717, 277);
            this.my_Text.TabIndex = 1;
            // 
            // showUri
            // 
            this.showUri.Location = new System.Drawing.Point(57, 339);
            this.showUri.Multiline = true;
            this.showUri.Name = "showUri";
            this.showUri.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.showUri.Size = new System.Drawing.Size(717, 265);
            this.showUri.TabIndex = 2;
            // 
            // bFindOutUrl
            // 
            this.bFindOutUrl.Location = new System.Drawing.Point(586, 13);
            this.bFindOutUrl.Name = "bFindOutUrl";
            this.bFindOutUrl.Size = new System.Drawing.Size(188, 37);
            this.bFindOutUrl.TabIndex = 3;
            this.bFindOutUrl.Text = "Find Out URL!";
            this.bFindOutUrl.UseVisualStyleBackColor = true;
            this.bFindOutUrl.Click += new System.EventHandler(this.button2_Click);
            // 
            // tbUrl
            // 
            this.tbUrl.Location = new System.Drawing.Point(57, 29);
            this.tbUrl.Name = "tbUrl";
            this.tbUrl.Size = new System.Drawing.Size(270, 20);
            this.tbUrl.TabIndex = 4;
            this.tbUrl.TextChanged += new System.EventHandler(this.tbUrl_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(57, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Input url";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 6;
            // 
            // FMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 616);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbUrl);
            this.Controls.Add(this.bFindOutUrl);
            this.Controls.Add(this.showUri);
            this.Controls.Add(this.my_Text);
            this.Controls.Add(this.bShow);
            this.Name = "FMain";
            this.Text = "All Urls!";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bShow;
        private System.Windows.Forms.TextBox my_Text;
        private System.Windows.Forms.TextBox showUri;
        private System.Windows.Forms.Button bFindOutUrl;
        private System.Windows.Forms.TextBox tbUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

